# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
import pandas as pd
from pymongo import MongoClient

mongo_ip = ""
with open('./mongo_ip.txt', 'r') as mongo_ip_config:
    mongo_ip = mongo_ip_config.readline().replace("'", "").replace("\n", "")
print("\n\n" + mongo_ip + "\n\n")
client = MongoClient(host=mongo_ip, port=27017)

df = pd.DataFrame(list(client.biopolymer["allfilms"].find({})))
compositions = ['amylose', 'amylopectin', 
                'casein', 'pectin', 'gelatin',
                'cellulose','chitosan', 
                'glycerol', 'sorbitol']
biography = ['PolymerID', 'FilmName', 'Journal', 'Year', '1stAuthor', 'doi']
properties = ['TensileStrength(MPa)', 'ElongationAtBreak(%)', 'ElasticModulus(MPa)', 
                'WaterVaporPermeability(gmm/(m^2daykPa))', 
                'WaterVaporTransimissionRate(g/(m^2day))']
tr = df[compositions + biography + properties].copy(deep=True)
tr["sum"] = tr['amylose'] + tr['amylopectin'] + tr['casein'] + tr['pectin'] + tr['gelatin'] + tr['cellulose'] + tr['chitosan']
tr = tr[tr["sum"] > 0.]

dropdown_labels_df = tr["PolymerID"] + ": " + tr["FilmName"].replace(0., "N/A").astype(str) + ", " + tr["1stAuthor"] + " et al., " + tr["Year"].astype(str) + ", " + tr["Journal"].replace(0., "N/A").astype(str) + ", DOI: " + tr['doi'].replace(0., "N/A").astype(str)
#dropdown_labels_df = tr.index

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import numpy as np

def gen_fig_water(_pd):
    fig = make_subplots(
        rows=2, cols=2,
        specs=[[{"type": "histogram"}, {"type": "histogram"}],
               [{"type": "scatter"}, {'type': "scatter"}]],
    )

    _pd = _pd[_pd['WaterVaporPermeability(gmm/(m^2daykPa))']!=0]
    _pd = _pd[_pd['WaterVaporTransimissionRate(g/(m^2day))']!=0]
    customdata = np.stack((_pd['doi'], #0
                            _pd['Journal'], #1
                            _pd['Year'], #2
                            _pd['1stAuthor'], #3
                            _pd['FilmName'], #4
                            _pd['WaterVaporPermeability(gmm/(m^2daykPa))'], #5
                            _pd['WaterVaporTransimissionRate(g/(m^2day))'], #6
                            _pd['PolymerID']), #7
                          axis=-1)

    #fig.add_trace(go.Scatter(x=_pd['PolymerID'], 
                         #y=_pd['WaterVaporPermeability(gmm/(m^2daykPa))'],
                         #mode="markers",
                         #customdata=customdata,
                         #hovertemplate="<b>%{customdata[7]}</b><br><br>" +
                        #"Water Vapor Permeability (WVP): %{customdata[5]} g mm/(m^2 day kPa)<br>"+
                        #"Published Name: %{customdata[4]}<br>" +
                        #"DOI: %{customdata[0]}<br>" +
                        #"Journal: %{customdata[1]}<br>" +
                        #"Year: %{customdata[2]}<br>" +
                        #"Author: %{customdata[3]}, et al.<br>" +
                        #"<extra></extra>",),
                  #row=1, col=1)


    fig.add_trace(
        go.Histogram(
            x=_pd['WaterVaporPermeability(gmm/(m^2daykPa))'], 
            marker=dict(color="#636EFA"),
        ),
        row=1, col=1,
    )
    fig.add_trace(
        go.Histogram(
            x=_pd['WaterVaporPermeability(gmm/(m^2daykPa))'], 
            marker=dict(color="#EF553B"),
        ),
        row=1, col=2,
    )
    

    #fig.add_trace(go.Scatter(x=_pd['PolymerID'], 
                         #y=_pd['WaterVaporTransimissionRate(g/(m^2day))'],
                         #mode="markers",
                         #customdata=customdata,
                         #hovertemplate="<b>%{customdata[7]}</b><br><br>" +
                        #"Water Vapor Transimission Rate (WVTR): %{customdata[6]} g/(m^2 day)<br>"+
                        #"Published Name: %{customdata[4]}<br>" +
                        #"DOI: %{customdata[0]}<br>" +
                        #"Journal: %{customdata[1]}<br>" +
                        #"Year: %{customdata[2]}<br>" +
                        #"Author: %{customdata[3]}, et al.<br>" +
                        #"<extra></extra>",),
                  #row=1, col=2)
    color_2d_red = ((_pd['WaterVaporPermeability(gmm/(m^2daykPa))'] - _pd['WaterVaporPermeability(gmm/(m^2daykPa))'].min()) / (_pd['WaterVaporPermeability(gmm/(m^2daykPa))'].max() - _pd['WaterVaporPermeability(gmm/(m^2daykPa))'].min()) * 255).astype(int).astype(str)
    color_2d_green = ((_pd['WaterVaporTransimissionRate(g/(m^2day))'] - _pd['WaterVaporTransimissionRate(g/(m^2day))'].min()) / (_pd['WaterVaporTransimissionRate(g/(m^2day))'].max() - _pd['WaterVaporTransimissionRate(g/(m^2day))'].min()) * 255).astype(int).astype(str)
    color_2d_blue = (255 - ((color_2d_red.astype(int) + color_2d_green.astype(int)) / 2.).astype(int)).astype(str)
    color_2d = "rgba(" + color_2d_red + ", " + color_2d_green + ", " + color_2d_blue + ", 1)"
    #print(color_2d_red, color_2d_green, color_2d_blue)
    fig.add_trace(go.Scatter(x=_pd['WaterVaporPermeability(gmm/(m^2daykPa))'], 
                             y=_pd['WaterVaporTransimissionRate(g/(m^2day))'], 
                             mode="markers",
                             customdata=customdata,
                             hovertemplate="<b>%{customdata[7]}</b><br><br>" +
                            "Water Vapor Permeability (WVP): %{customdata[5]} g mm/(m^2 day kPa)<br>"+
                            "Water Vapor Transimission Rate (WVTR): %{customdata[6]} g/(m^2 day)<br>"+
                            "Published Name: %{customdata[4]}<br>" +
                            "DOI: %{customdata[0]}<br>" +
                            "Journal: %{customdata[1]}<br>" +
                            "Year: %{customdata[2]}<br>" +
                            "Author: %{customdata[3]}, et al.<br>" +
                            "<extra></extra>", 
                        marker=dict(color=color_2d),),
                  row=2, col=1)

    fig.update_xaxes(showticklabels=True)
    fig.update_xaxes(title_text='Water Vapor Permeability (WVP)', row=1, col=1)
    fig.update_xaxes(title_text='Water Vapor Transimission Rate (WVTR)', row=1, col=2)
    fig.update_yaxes(title_text='Count', row=1, col=1)
    fig.update_yaxes(title_text='Count', row=1, col=2)
    fig.update_xaxes(title_text='Water Vapor Permeability (WVP)', row=2, col=1)
    fig.update_yaxes(title_text='Water Vapor Transimission Rate (WVTR)', row=2, col=1)

    #fig.update_layout(scene = dict(
        #xaxis = dict(title='Tensile Strength(MPa)'),
        #yaxis = dict(title='Elongation At Break(%)'),
        #zaxis = dict(title='Elastic Modulus(MPa)'),))
    fig.update_layout(title_text="Bio-based Polymer Film Water Resistance Properties", height=700)
    fig.update_layout(template="seaborn",
                      showlegend=False, 
                      autosize=False,
                      width=1800,
                      height=1800,
                      )
    return fig

def gen_fig_mech(_pd):
    fig = make_subplots(
        rows=4, cols=2,
        specs=[[{"type": "histogram"}, {"type": "histogram"}],
               [{"type": "histogram"}, {"type": "scatter"}],
               [{"type": "scatter"}, {'type': "scatter"}],
               [{"type": "surface"}, {'type': "scatter"}]],
    )
    _pd = _pd[_pd['TensileStrength(MPa)']!=0]
    _pd = _pd[_pd['ElongationAtBreak(%)']!=0]
    _pd = _pd[_pd['ElasticModulus(MPa)']!=0]
    customdata = np.stack((_pd['doi'], #0
                            _pd['Journal'], #1
                            _pd['Year'], #2
                            _pd['1stAuthor'], #3
                            _pd['FilmName'], #4
                            _pd['TensileStrength(MPa)'], #5
                            _pd['ElongationAtBreak(%)'], #6
                            _pd['ElasticModulus(MPa)'], #7
                            _pd['PolymerID']), #8
                          axis=-1)


    fig.add_trace(
        go.Histogram(
            x=_pd['TensileStrength(MPa)'], 
            marker=dict(color="#636EFA"),
        ),
        row=1, col=1,
    )
    
    fig.add_trace(
        go.Histogram(
            x=_pd['ElongationAtBreak(%)'], 
            marker=dict(color="#EF553B"),
        ),
        row=1, col=2,
    )


    fig.add_trace(
        go.Histogram(
            x=_pd['ElasticModulus(MPa)'], 
            marker=dict(color="#00CC96"),
        ),
        row=2, col=1,
    )
    

    fig.add_trace(go.Scatter(x=_pd['TensileStrength(MPa)'], 
                         y=_pd['ElongationAtBreak(%)'],
                         mode="markers",
                         customdata=customdata,
                         hovertemplate="<b>%{customdata[8]}</b><br><br>" +
                        "Tensile Strength: %{customdata[5]} MPa<br>"+
                        "Elongation at Break: %{customdata[6]} &#37;<br>"+
                        "Elastic Modulus: %{customdata[7]} MPa;<br>"+
                        "Published Name: %{customdata[4]}<br>" +
                        "DOI: %{customdata[0]}<br>" +
                        "Journal: %{customdata[1]}<br>" +
                        "Year: %{customdata[2]}<br>" +
                        "Author: %{customdata[3]}, et al.<br>" +
                        "<extra></extra>", 
                        marker=dict(color="#AB63FA"),),
                  row=2, col=2)

    fig.add_trace(go.Scatter(x=_pd['TensileStrength(MPa)'], 
                         y=_pd['ElasticModulus(MPa)'],
                         mode="markers",
                         customdata=customdata,
                         hovertemplate="<b>%{customdata[8]}</b><br><br>" +
                        "Tensile Strength: %{customdata[5]} MPa<br>"+
                        "Elongation at Break: %{customdata[6]} &#37;<br>"+
                        "Elastic Modulus: %{customdata[7]} MPa;<br>"+
                        "Published Name: %{customdata[4]}<br>" +
                        "DOI: %{customdata[0]}<br>" +
                        "Journal: %{customdata[1]}<br>" +
                        "Year: %{customdata[2]}<br>" +
                        "Author: %{customdata[3]}, et al.<br>" +
                        "<extra></extra>", 
                        marker=dict(color="#FFA15A"),),
                  row=3, col=1)

    fig.add_trace(go.Scatter(x=_pd['ElongationAtBreak(%)'], 
                         y=_pd['ElasticModulus(MPa)'],
                         mode="markers",
                         customdata=customdata,
                         hovertemplate="<b>%{customdata[8]}</b><br><br>" +
                        "Tensile Strength: %{customdata[5]} MPa<br>"+
                        "Elongation at Break: %{customdata[6]} &#37;<br>"+
                        "Elastic Modulus: %{customdata[7]} MPa;<br>"+
                        "Published Name: %{customdata[4]}<br>" +
                        "DOI: %{customdata[0]}<br>" +
                        "Journal: %{customdata[1]}<br>" +
                        "Year: %{customdata[2]}<br>" +
                        "Author: %{customdata[3]}, et al.<br>" +
                        "<extra></extra>", 
                        marker=dict(color="#19D3F3"),),
                  row=3, col=2)

    color_3d_red = ((_pd['TensileStrength(MPa)'] - _pd['TensileStrength(MPa)'].min()) / (_pd['TensileStrength(MPa)'].max() - _pd['TensileStrength(MPa)'].min()) * 255).astype(int).astype(str)
    color_3d_green = ((_pd['ElongationAtBreak(%)'] - _pd['ElongationAtBreak(%)'].min()) / (_pd['ElongationAtBreak(%)'].max() - _pd['ElongationAtBreak(%)'].min()) * 255).astype(int).astype(str)
    color_3d_blue = (255 - ((_pd['ElasticModulus(MPa)'] - _pd['ElasticModulus(MPa)'].min()) / (_pd['ElasticModulus(MPa)'].max() - _pd['ElasticModulus(MPa)'].min()) * 255)).astype(int).astype(str)

    print(color_3d_red, color_3d_green, color_3d_blue)
    color_3d = "rgba(" + color_3d_red + ", " + color_3d_green + ", " + color_3d_blue + ", 1)"
    fig.add_trace(go.Scatter3d(x=_pd['TensileStrength(MPa)'], 
                         y=_pd['ElongationAtBreak(%)'], 
                         z=_pd['ElasticModulus(MPa)'],
                         customdata=customdata,
                         mode='markers',
                         hovertemplate="<b>%{customdata[8]}</b><br><br>" +
                        "Tensile Strength: %{customdata[5]} MPa<br>"+
                        "Elongation at Break: %{customdata[6]} &#37;<br>"+
                        "Elastic Modulus: %{customdata[7]} MPa;<br>"+
                        "Published Name: %{customdata[4]}<br>" +
                        "Journal: %{customdata[0]}<br>" +
                        "Year: %{customdata[1]}<br>" +
                        "Author: %{customdata[2]}, et al.<br>" +
                        "<extra></extra>", 
                        marker=dict(color=color_3d, size=10)),
                  row=4, col=1)

    fig.update_xaxes(showticklabels=True)
    fig.update_xaxes(title_text='Tensile Strength (MPa)', row=1, col=1)
    fig.update_xaxes(title_text='Elongation at Break (%)', row=1, col=2)
    fig.update_xaxes(title_text='Elastic Modulus (MPa)', row=2, col=1)
    fig.update_yaxes(title_text='Count', row=1, col=1)
    fig.update_yaxes(title_text='Count', row=1, col=2)
    fig.update_yaxes(title_text='Count', row=2, col=1)
    fig.update_yaxes(title_text='Elongation at Break (%)', row=2, col=2)
    fig.update_xaxes(title_text='Tensile Strength (MPa)', row=2, col=2)
    fig.update_yaxes(title_text='Elongation at Break (%)', row=2, col=2)
    fig.update_xaxes(title_text='Tensile Strength (MPa)', row=3, col=1)
    fig.update_yaxes(title_text='Elastic Modulus (MPa)', row=3, col=1)
    fig.update_xaxes(title_text='Elongation at Break (%)', row=3, col=2)
    fig.update_yaxes(title_text='Elastic Modulus (MPa)', row=3, col=2)

    fig.update_layout(scene = dict(
        xaxis = dict(title='Tensile Strength (MPa)'),
        yaxis = dict(title='Elongation At Break (%)'),
        zaxis = dict(title='Elastic Modulus (MPa)'),))
    fig.update_layout(title_text="Bio-based Polymer Film Mechanical Properties", height=700)
    fig.update_layout(template="seaborn",
                      showlegend=False, 
                      autosize=False,
                      width=1800,
                      height=3600,
                      )
    return fig


@app.callback(Output('recipe-graph', 'figure'), 
              [Input('recipe-dropdown', 'value')])
def gen_fig_recipe(option_value):
    #d = str(option_value).split(":")[0].replace("pdb-", "")
    if str(option_value).isdigit():
        i = int(option_value)
    else:
        i = -1
    #i = option_value
    if i < 0:
        fig = make_subplots(rows=1, cols=1, specs=[[{"type": "domain"}]])
        return fig
    else: 
        recipe = tr.loc[i, compositions]
        recipe = recipe[recipe>0.].to_frame().T

        fig = make_subplots(rows=2, cols=1, specs=[[{"type": "domain"}], 
                                                   [{"type": "polar"}]])
        fig.add_trace(
            go.Pie(
                labels=recipe.columns, 
                values=recipe.to_numpy().flatten(),
                name=tr.at[i, "PolymerID"], 
                text=(((recipe.T*10000.).astype(int)/100).astype(str) + "% Tot. Wt.").to_numpy().flatten(),
            ), 
            row=1, 
            col=1,
        )
        fig.update_traces(hoverinfo='label+percent', textinfo='label+text', 
            row=1, 
            col=1,)
        fig.update(layout_title_text=tr.at[i, "PolymerID"],
                   layout_showlegend=True, )
        fig.update_layout(title_text="Bio-based Polymer Film Composition Breakdown", height=700)

        r = []
        import numpy as np
        for prop in properties:
            r.append(np.sqrt(np.sqrt((tr.at[i, prop] - tr[prop].min() + 0.1) / (tr[prop].max() - tr[prop].min() + 0.1))))

        fig.add_trace(
            go.Scatterpolar(
                r=r,
                theta=properties,
                fill='toself',
                name=tr.at[i, "PolymerID"],
            ), 
            row=2, 
            col=1,
        )
        fig.update_layout(
            autosize=False,
            width=900,
            height=900, 
        )
        fig.update_layout(
            polar=dict(
                radialaxis=dict(
                    visible=True,
                    range=[0, 1.],
                    showticklabels=False,
                ),
            ),
            showlegend=False,
        )
    return fig
"""
def gen_fig_recipe(i):
    if i < 0:
        fig = make_subplots(rows=1, cols=1, specs=[[{"type": "scatter"}]])
        fig.update_layout(title_text="Bio-based Polymer Film Composition Breakdown", height=700)
        fig.update_layout(
            autosize=False,
            width=700,
            height=700,
        )
        return fig
    else: 
        recipe = tr.loc[i, compositions]
        recipe = recipe[recipe>0.].to_frame().T

        fig = make_subplots(rows=1, cols=1, specs=[[{"type": "domain"}]])
        fig.add_trace(
            go.Pie(
                labels=recipe.columns, 
                values=recipe.to_numpy().flatten(),
                name=tr.at[i, "PolymerID"], 
                text=(((recipe.T*10000.).astype(int)/100).astype(str) + "% Tot. Wt.").to_numpy().flatten(),
            ), 
            row=1, 
            col=1,
        )
        fig.update_traces(hoverinfo='label+percent', textinfo='label+text')
        fig.update(layout_title_text=tr.at[i, "PolymerID"],
                   layout_showlegend=True)
        fig.update_layout(title_text="Bio-based Polymer Film Composition Breakdown", height=700)
        fig.update_layout(
            autosize=False,
            width=700,
            height=700,
        )
    return fig
"""
index_page = html.Div([
    html.H1(children='Food Waste Bioplastic Database Prototype'),
    html.Div(children='''
        Hello, this is a demonstration of plotly/dash-based web interface with a live database server in the background. 
    '''),
    html.Br(),
    dcc.Link('Composition Viewer', href='/recipe'),
    html.Br(),
    dcc.Tabs(
        id='tabs-propertyTypes', 
        value='tab-mech', 
        children=[
            dcc.Tab(label='Mechanical', value='tab-mech'),
            dcc.Tab(label='Water Resistance', value='tab-water'),
        ]
    ),
    html.Div(id='tabs-propertyTypes-content')
])

recipe_layout = html.Div([
    html.H1("Composition Viewer"),
    html.Br(),
    dcc.Link('Property Viewer', href='/'),
    html.Br(),
    dcc.Dropdown(
        id='recipe-dropdown', 
        options=[{'label': dropdown_labels_df[j], 
            'value': j} for j in dropdown_labels_df.index],
        searchable=True,
        #placeholder="Select a biopolymer recipe ID",
    ),
    dcc.Graph(
        id='recipe-graph',
    ),
],
style={"width": "100%"},)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div([index_page, recipe_layout], id='page-content')
])

@app.callback(Output('tabs-propertyTypes-content', 'children'),
              Input('tabs-propertyTypes', 'value'))
def render_content(tab):
    if tab == 'tab-mech':
        return html.Div([
            dcc.Graph(
                id='mechanical',
                figure=gen_fig_mech(df)
            )
        ])
    #elif tab == 'tab-water':
    else:
        return html.Div([
            dcc.Graph(
                id='mechanical',
                figure=gen_fig_water(df)
            )
        ])


# Update the index
@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/recipe':
        return recipe_layout
    else:
        return index_page

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', debug=True)