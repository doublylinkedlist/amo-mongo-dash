for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=biopolymer_dash_1" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=biopolymer_dash_1" -q`) do (docker container rm %%x)
for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=biopolymer_db_1" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=biopolymer_db_1" -q`) do (docker container rm %%x)
for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=biopolymer_db_seed_1" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=biopolymer_db_seed_1" -q`) do (docker container rm %%x)
for /f "usebackq delims=" %%x in (`docker ps -a -f "ancestor=biopolymer_jupyter_1" -q`) do (docker kill %%x && docker stop %%x)
for /f "usebackq delims=" %%x in (`docker container ls -a -f "ancestor=biopolymer_jupyter_1" -q`) do (docker container rm %%x)

docker-compose rm -f
for /f "usebackq delims=" %%x in (`docker volume ls -qf "dangling=true"`) do (@echo off && docker volume rm %%x>NUL)
docker image prune -f
docker-compose build

start cmd /k "docker-compose up"
:label9
docker ps -f "NAME=biopolymer_db_1"|find /i "biopolymer_db_1" >NUL
if errorlevel 1 (timeout/t 1 /nobreak>nul && goto :label9)
echo db is up
timeout/t 5 /nobreak>NUL
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' biopolymer_db_1 > .\app\mongo_ip.txt

echo db_seed is not launched
:label0
docker ps -f "NAME=biopolymer_db_seed_1"|find /i "biopolymer_db_seed_1" >NUL

::if errorlevel 1 (timeout/t 1 /nobreak>nul && goto :label0)
echo.
echo ##################################################
echo ##################################################
echo db_seed is launched, database is being initialized
echo ##################################################
echo ##################################################
echo.
echo db is not ready
:label
docker ps -f "NAME=biopolymer_db_seed_1"|find /i "biopolymer_db_seed_1" >NUL
if errorlevel 1 (goto :label1)
timeout/t 1 /nobreak>nul
goto :label
echo.
:label1
docker ps -a -f "exited=0"|find /i "biopolymer_db_seed_1" >NUL
if errorlevel 1 (timeout/t 1 /nobreak>nul && goto :label2)

echo.
echo ##################################################
echo ##################################################
ECHO db is initialized and ready for connection
echo ##################################################
echo ##################################################
echo.
timeout/t 3 /nobreak>NUL
docker exec -it biopolymer_jupyter_1 bash ./jupyter_list.sh
timeout/t 1 /nobreak>NUL
for /f "delims=" %%x in (./jupyter_token.txt) do set jupyter_token=%%x
set jupyterurl=http://localhost:8870%jupyter_token:~22,-8%
timeout/t 1 /nobreak>nul
start "" %jupyterurl%

timeout/t 1 /nobreak>NUL
start "" http://127.0.0.1:8886/
goto :label3
:label2
echo ##################################################
echo ##################################################
echo db is not functioning, please restart the application
echo ##################################################
echo ################################################## 
PAUSE
:label3
cmd /k